#! /usr/bin/env python3

import argparse
import io

import froeling
import requests
import flask

parser = argparse.ArgumentParser("Prometheus exporter for Fröling facility")
parser.add_argument("login", help="Fröling user login")
parser.add_argument("password", help="Fröling user password")
parser.add_argument("--port", "-p", type=int, help="exporter listening port", default=11111)
parser.add_argument("--language", "-l", help="language")
args = parser.parse_args()

token = froeling.Token(args.login, args.password)
app = flask.Flask(__name__)


@app.route("/metrics")
def metrics():
    global token
    metrics = froeling.scrapmetrics(token=token, language=args.language)

    metricdict = {}
    for facility, metriclist in metrics.items():
        for metric in metriclist:
            try:
                value = float(metric['value'])
            except:
                continue
            metric['help'] = f"id={metric['id']} {metric['displayName']}"
            if metric['unit']:
                metric['help'] += f" unit={metric['unit']}"
            metric['facility'] = facility
            metricdict.setdefault(metric['name'], []).append(metric)
    
    response = io.StringIO()
    for metrics in metricdict.values():
        first = True
        for metric in metrics:
            name = metric['name']
            help = metric['help']
            type = metric['parameterType']
            facility = metric['facility']
            value = metric['value']
            if first:
                print(f"# HELP {name} {help}", file=response)
                if type == 'NumValueObject':
                    print(f"# TYPE {name} gauge", file=response)
            print(f'{name}{{facility="{facility}"}} {value}', file=response)

    return flask.Response(response.getvalue(), content_type='text/plain')

app.run(port=args.port)
